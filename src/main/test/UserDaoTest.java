import com.jthinker.bean.UserWrapper;
import com.jthinker.dao.UserDao;
import org.junit.Test;

/**
 * Created by chen.gang on 2014/11/23.
 */
public class UserDaoTest extends BaseTest
{
    @Test
    public void test()
    {
        UserDao userDao = CONTEXT.getBean(UserDao.class);
        UserWrapper userWrapper = userDao.getUser("admin", "ceb4f32325eda6142bd65215f4c0f371");

        System.out.println(userWrapper);
    }
}
