import com.jthinker.dao.BaseDao;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by chen.gang on 2014/11/22.
 */
public class BaseDaoTest extends BaseTest
{
    private BaseDao baseDao = CONTEXT.getBean(BaseDao.class);

    private JdbcTemplate jdbcTemplate = CONTEXT.getBean(JdbcTemplate.class);

    class BaseRowMapper implements RowMapper<Map<String, Object>>
    {
        @Override
        public Map<String, Object> mapRow(ResultSet rs, int rowNum)
                throws SQLException
        {
            ResultSetMetaData metaData = rs.getMetaData();
            int colCount = metaData.getColumnCount();

            Map<String, Object> row = new HashMap<String, Object>();

            for (int i = 1; i <= colCount; i++)
            {
                String colName = metaData.getColumnLabel(i) != null ? metaData
                        .getColumnLabel(i).toLowerCase() : metaData
                        .getColumnName(i).toLowerCase();
                Object val = rs.getObject(i);
                row.put(colName, val);
            }
            return row;
        }
    }

    @Test
    public void testQuery()
    {
        String sql = "SELECT * FROM sys_account";

        List<Map<String, Object>> list = baseDao.query(sql);
        System.out.println(list.get(0).get("account"));

        List<Map<String, Object>> list2 = jdbcTemplate.query(sql, new BaseRowMapper());

        System.out.println(list2.get(0).get("nickname").toString());
    }
}
