import com.caucho.hessian.client.HessianProxyFactory;
import com.jthinker.service.TicketService;
import org.junit.Test;

import java.net.MalformedURLException;

/**
 * Created by chen.gang on 2015/1/24.
 */
public class HessianClientTest
{
    @Test
    public void test() throws MalformedURLException
    {
        String url = "http://127.0.0.1:8080/BoringSSO/api/ticketService";
        HessianProxyFactory factory = new HessianProxyFactory();
        TicketService ticketService = (TicketService) factory.create(TicketService.class, url);
        System.out.println(ticketService.validateST("123"));
    }
}
