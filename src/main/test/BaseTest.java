import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by chen.gang on 2014/11/22.
 */
public class BaseTest
{
    protected static final ApplicationContext CONTEXT = new ClassPathXmlApplicationContext("/dao.xml", "/service.xml");
}
