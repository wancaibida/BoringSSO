package com.jthinker.service;

import com.jthinker.bean.UserWrapper;

/**
 * Created by chen.gang on 2014/11/22.
 */
public interface UserService
{
    UserWrapper getUser(String account, String saltPassword);

    void updateLoginTime(long id, String time);

}
