package com.jthinker.service.impl;

import com.jthinker.base.constant.Constant;
import com.jthinker.base.utils.CacheUtils;
import com.jthinker.bean.UserWrapper;
import com.jthinker.service.TicketService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 票据service
 * Created by chen.gang on 2015/1/24.
 */
public class TicketServiceImpl implements TicketService
{
    private static final Logger LOGGER = LoggerFactory.getLogger(TicketService.class);

    @Override
    public UserWrapper validateST(String st)
    {
        String tgt = CacheUtils.get(Constant.TICKET_CACHE_NAME, st);
        if (StringUtils.isBlank(tgt))
        {
            return null;
        }
        else
        {
            CacheUtils.remove(Constant.TICKET_CACHE_NAME, st);
        }

        Long userId = CacheUtils.get(Constant.TICKET_CACHE_NAME, tgt);
        if (userId == null)
        {
            return null;
        }

        UserWrapper wrapper = CacheUtils.get(Constant.USER_CACHE_NAME, userId);

        LOGGER.info("ServiceTicket {}, UserWrapper {}", st, wrapper);

        return wrapper;
    }
}
