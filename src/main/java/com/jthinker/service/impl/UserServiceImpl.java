package com.jthinker.service.impl;

import com.jthinker.bean.UserWrapper;
import com.jthinker.dao.UserDao;
import com.jthinker.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by chen.gang on 2014/11/22.
 */
@Service
public class UserServiceImpl implements UserService
{
    @Resource
    private UserDao userDao;

    @Override
    public UserWrapper getUser(String account, String saltPassword)
    {
        return userDao.getUser(account, saltPassword);
    }

    @Override
    public void updateLoginTime(long id, String time)
    {
        userDao.updateLoginTime(id, time);
    }
}
