package com.jthinker.service;

import com.jthinker.bean.UserWrapper;

/**
 * Created by chen.gang on 2015/1/24.
 */
public interface TicketService
{
    UserWrapper validateST(String st);
}
