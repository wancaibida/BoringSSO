package com.jthinker.base.ex;

/**
 * Created by chen.gang on 2014/11/22.
 */
public class UserException extends RuntimeException
{
    public UserException(String message)
    {
        super(message);
    }
}
