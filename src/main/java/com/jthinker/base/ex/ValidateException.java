package com.jthinker.base.ex;

/**
 * Created by chen.gang on 2014/11/22.
 */
public class ValidateException extends UserException
{
    public ValidateException(String message)
    {
        super(message);
    }
}
