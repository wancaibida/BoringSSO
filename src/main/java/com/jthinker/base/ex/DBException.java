package com.jthinker.base.ex;

/**
 * Created by chen.gang on 2014/11/22.
 */
public class DBException extends RuntimeException
{
    public DBException(Throwable cause)
    {
        super(cause);
    }
}
