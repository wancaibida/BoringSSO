package com.jthinker.base.utils;

import org.apache.commons.lang3.time.FastDateFormat;

import java.util.Date;

/**
 * Created by chen.gang on 2014/12/26.
 */
public class DateUtils
{
    private static final FastDateFormat DEFAULT_FORMAT = FastDateFormat.getInstance("yyyy-MM-dd HH:mm:ss");

    /**
     * 返回当前时间字符串 2014-12-26 20:28:02
     *
     * @return
     */
    public static String getNowTime()
    {
        return DEFAULT_FORMAT.format(new Date());
    }

    public static void main(String[] args)
    {
        System.out.println(getNowTime());
    }
}
