package com.jthinker.base.constant;

import java.nio.charset.Charset;

/**
 * Created by chen.gang on 2014/11/8.
 */
public class Constant
{
    public static final String TGT = "TGT";

    public static final String ST = "ST";

    public static final String TGC = "TGC";

    public static final String DEFAULT_ENCODING = "UTF-8";

    public static final Charset DEFAULT_CHARSET = Charset.forName(DEFAULT_ENCODING);

    public static final String INDEX = "http://www.google.com";

    //票据缓存名
    public static final String TICKET_CACHE_NAME = "ticketCache";

    //用户缓存名
    public static final String USER_CACHE_NAME = "userCache";

    //登出用的缓存
    public static final String LOGOUT_CACHE = "logoutCache";

    public static final String MODULUS = "modulus";
    public static final String EXPONENT = "exponent";

    public static final String SSO_SESSION = "ssoSession";

    /**
     * 跳转的URL字段名称
     */
    public static final String SERVICE_NAME = "service";

    /**
     * 登出广播
     */
    public static final String LOG_OUT_BROADCAST = "ssoLogOut";
}
