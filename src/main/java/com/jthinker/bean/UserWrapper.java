package com.jthinker.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 用户信息类
 * Created by chen.gang on 2014/11/8.
 */
public class UserWrapper implements Serializable
{
    private long id;
    private String account;
    private String nickname;
    private String lastTime;
    private String st;

    public Map<String, Object> attrs = new HashMap<String, Object>();

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getAccount()
    {
        return account;
    }

    public void setAccount(String account)
    {
        this.account = account;
    }

    public String getNickname()
    {
        return nickname;
    }

    public void setNickname(String nickname)
    {
        this.nickname = nickname;
    }

    public Map<String, Object> getAttrs()
    {
        return attrs;
    }

    public void setAttrs(Map<String, Object> attrs)
    {
        this.attrs = attrs;
    }

    public Object setAttr(String name, Object val)
    {
        return attrs.put(name, val);
    }

    public <T> T getAttr(String name)
    {
        return (T) attrs.get(name);
    }

    public String getLastTime()
    {
        return lastTime;
    }

    public void setLastTime(String lastTime)
    {
        this.lastTime = lastTime;
    }

    public String getSt()
    {
        return st;
    }

    public void setSt(String st)
    {
        this.st = st;
    }

}
