package com.jthinker.dao.impl;

import com.jthinker.bean.UserWrapper;
import com.jthinker.dao.BaseDao;
import com.jthinker.dao.UserDao;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

/**
 * Created by chen.gang on 2014/11/22.
 */
@Component
public class UserDaoImpl implements UserDao
{
    @Resource
    private BaseDao baseDao;

    @Override
    public UserWrapper getUser(String account, String saltPassword)
    {
        Map<String, Object> record = baseDao.read("SELECT * FROM sys_account WHERE account=? AND `password`=?", account, saltPassword);
        if (record == null || record.isEmpty())
        {
            return null;
        }

        UserWrapper userWrapper = new UserWrapper();

        userWrapper.setId(((Number) record.get("id")).longValue());
        userWrapper.setAccount(record.get("account").toString());
        userWrapper.setNickname(record.get("nickname").toString());
        userWrapper.setLastTime(record.get("last_time") != null ? record.get("last_time").toString() : null);

        return userWrapper;
    }

    @Override
    public void updateLoginTime(long id, String time)
    {
        String sql = "UPDATE sys_account a SET a.`last_time`=? WHERE a.`id`=?";
        baseDao.update(sql, time, id);
    }
}
