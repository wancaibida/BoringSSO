package com.jthinker.dao;

import java.util.List;
import java.util.Map;

/**
 * Created by chen.gang on 2014/11/22.
 */
public interface BaseDao
{
    List<Map<String, Object>> query(String sql, Object... params);

    /**
     * 读一条记录
     *
     * @param sql
     * @param params
     * @return
     */
    Map<String, Object> read(String sql, Object... params);

    /**
     * 更新
     *
     * @param sql
     * @param params
     * @return
     */
    int update(String sql, Object... params);
}
