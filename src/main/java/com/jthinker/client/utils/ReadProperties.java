package com.jthinker.client.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by chen.gang on 2014/12/28.
 */
public class ReadProperties
{
    private static final Logger LOGGER = LoggerFactory.getLogger(ReadProperties.class);

    private static final String EMPTY = "";

    private Properties props;

    public ReadProperties(String path)
    {
        InputStream in = ReadProperties.class.getResourceAsStream(path);
        props = new Properties();
        try
        {
            props.load(in);
        }
        catch (IOException e)
        {
            e.printStackTrace();
            LOGGER.error("Exception: {}", e);
            throw new RuntimeException(e);
        }
    }

    public int getInt(String name)
    {
        String str = getValue(name);
        return EMPTY.equals(str) ? -1 : Integer.parseInt(str);
    }

    public String getValue(String name)
    {
        Object o = props.get(name);
        return o == null ? EMPTY : o.toString();
    }
}
