$(function ()
{
    window.Utils = {};

    Utils.showDialog = function (options)
    {
        var dialog = [];
        dialog.push(" <div id=\"dialog\" title=\"" + options.title + "\">");
        dialog.push("<p>" + options.message + "</p>");
        dialog.push("</div>");
        var html = dialog.join("");

        $(html).dialog(options);
    };

    Utils.showError = function (message, callback)
    {
        var cb = callback || function ()
            {
                $(this).dialog("close");
            };

        var options = {
            title : '错误',
            message : message,
            resizable : false,
            modal : true,
            buttons : {
                "确认" : cb
            }
        };

        this.showDialog(options);
    };

    Utils.showSuccess = function (message, callback)
    {
        var cb = callback || function ()
            {
                $(this).dialog("close");
            };

        var options = {
            title : '提示',
            message : message,
            resizable : false,
            modal : true,
            buttons : {
                "确认" : cb
            }
        };

        this.showDialog(options);
    };

    Utils.showConfirm = function (message, confirmCallback, cancelCallback)
    {
        var defaultCallback = function ()
        {
            $(this).dialog("close");
        };

        confirmCallback = confirmCallback || defaultCallback;
        cancelCallback = cancelCallback || defaultCallback;

        var args = [];

        for (var i = 3, len = arguments.length; i < len; i++)
        {
            args.push(arguments[i]);
        }

        var options = {
            title : '提示',
            message : message,
            resizable : false,
            modal : true,
            buttons : {
                "确认" : confirmCallback,
                "取消" : cancelCallback
            }
        };

        this.showDialog(options);
    };
});