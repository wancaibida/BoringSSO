<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<c:set value="<%=basePath%>" var="basePath"/>

<script type="text/javascript" src="<%=basePath%>resources/js/base/jquery-1.11.1.js"></script>
<script type="text/javascript" src="<%=basePath%>resources/js/base/jquery.validate.js"></script>
<script type="text/javascript" src="<%=basePath%>resources/js/base/security.js"></script>
<script type="text/javascript" src="<%=basePath%>resources/js/base/jquery-ui-1.11.2/jquery-ui.min.js"></script>
<script type="text/javascript" src="<%=basePath%>resources/js/base/base64.min.js"></script>
<script type="text/javascript" src="<%=basePath%>resources/js/base/Utils.js"></script>


<link rel="stylesheet" href="<%=basePath%>resources/js/base/jquery-ui-1.11.2/jquery-ui.min.css">
<link rel="stylesheet" href="<%=basePath%>resources/js/base/jquery-ui-1.11.2/jquery-ui.structure.min.css">
<link rel="stylesheet" href="<%=basePath%>resources/js/base/jquery-ui-1.11.2/jquery-ui.theme.min.css">

<link rel="stylesheet" href="<%=basePath%>resources/css/base/base.css">

<script type="text/javascript">
    var basePath = '${basePath}';
    var modulus = '${applicationScope.modulus}';
    var exponent = '${applicationScope.exponent}';
    var key = RSAUtils.getKeyPair(exponent, '', modulus);
</script>
