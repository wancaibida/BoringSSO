<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
    <title>登录</title>
    <%@include file="/pages/base.jsp" %>
    <script type="text/javascript" src="<%=basePath%>resources/js/sso/login.js"></script>
</head>
<body>

<form method="post" id="loginForm">
    <input type="hidden" name="service" id="service" value="${param.service}">

    用户名:<input type="text" name="account" id="account"/>
    密码:<input type="password" name="password" id="password"/>

    <input type="submit" name="login" id="login" value="登录">
</form>


</body>
</html>
